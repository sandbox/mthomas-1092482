<?php
// $Id$

/**
 * @file
 *    Module to convert content with a given input format to markdown.
 *
 * Note: On the Markdown format the code filter should come after markdown because
 *    markdown has a built in ` ` tag for <code> </code> which we can then convert
 *    to a div and display properly.
 */

/**
 * Implementation of hook_perm().
 */
function markdownify_perm() {
  return array(
     'use markdownify',
  );
}

/**
 * Implementation of hook_menu().
 */
function markdownify_menu() {
  $items = array();
  $items['markdownify'] = array(
    'title' => 'Markdownify',
    'page callback' => 'markdownify_text_content',
    'description' => "Convert HTML content into Markdown.",
    'access arguments' => array('use markdownify'),
  );
  return $items;
}

/**
 * Defines content for Markdownify conversion page
 */
function markdownify_text_content() {
  $output = '<h1>Markdownify</h1><br />';
  $output .= drupal_get_form(markdownify_text_form);
  return $output;
}

/**
 * Defines form for Markdownify conversion.
 */
function markdownify_text_form() {
  $form['html-content'] = array(
    '#type' => 'textarea', 
    '#title' => t('HTML Content'), 
   // '#default_value' => $object['html-content'],
    '#required' => TRUE
);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Convert')
  );

  return $form;
}

/**
 * Prepare and execute conversion.
 */
function markdownify_text_form_submit($form, &$form_state) {
    $oldtext = $form_state['values']['html-content'];
    $newtext = markdownify_convert($oldtext);
    drupal_set_message("<pre>" . $newtext . "</pre>");
}

function markdownify_convert($htmlContent) {
 
  require_once drupal_get_path('module', 'markdownify') .'/markdownify/markdownify.php';
  require_once drupal_get_path('module', 'markdownify') .'/markdownify/markdownify_extra.php';
    
  $md = new Markdownify_Extra;
  
  //remove html classes
  $htmlContent = preg_replace('/(height|class|width|cellspacing|cellpadding|border)=".*?"/', '', $htmlContent);  
 
  //TODO: strip whitespace before and after <td>  
  $htmlContent = preg_replace('/>\s+</', "><", $htmlContent);  
 
  //remove html attributes other than img and href
  //$htmlContent = preg_replace("/<([a-z][a-z0-9]*)(?:[^>]*(\s(href|src)=['\"][^'\"]*['\"]))?[^>]*?(\/?)>/i",'<$1$2$3>', $htmlContent); 

  //remove html classes
  $node->body = preg_replace('/class=".*?"/', '', $node->body);
 
  $htmlContent = $md->parseString(check_markup($htmlContent, FALSE));

  return $htmlContent;
}

/**
 * Action for Markdown conversion of node bodies
 */
function markdownify_action_info() {
  return array(
    'markdownify_convert_node' => array(
      'description' => 'Convert node body to Markdown.',
      'type' => 'node',
      'configurable' => TRUE,
      'hooks' => array(
	         'nodeapi' => array('presave', 'insert', 'update', 'view'),
	         'comment' => array('insert'),
	       )
	    )
	);
}

/**
 * Function for Markdown conversion of node bodies
 */
function markdownify_convert_node_form($context) {
	//get a list of input formats
  	$format_list = filter_formats();
  	$formats = array();
  	foreach($format_list as $format) {
    	$formats[$format->format] = $format->name;
  	}

	$form['newformat'] = array(
	    '#title' => t('New Input Format'),
	    '#type' => 'select',
	    '#title' => t('Target Format'),
		'#description' => t('<strong>This format must have Markdown enabled as an input filter.</strong>'),
		'#required' => TRUE,
		'#options' => $formats,
	);
		
	  return $form;
}

/**
 * Submit handler for Markdown conversion of node bodies
 */
markdownify_convert_node_submit($form, &$form_state) {
  $newformatid = $form_state['values']['newformat'];
  return array("newformat" => $newformatid);
}

/**
 * Conversion function for conversion of node bodies from HTML to Markdown
 */
markdownify_convert_node(&$object, $context = array()) {
  require_once drupal_get_path('module', 'aashe_processor') .'/markdownify/markdownify.php';
  $node = $object;
  $md = new markdownify;
  $newformat = $context['newformat'];
  $node->format = $newformat;
  //remove html classes
  $node->body = preg_replace('/class=".*?"/', '', $node->body);
  $node->body = $md->parseString(check_markup($node->body, $node->format, FALSE));
  $node->teaser = node_teaser($node->body, $node->format); 
  node_save($node);
}